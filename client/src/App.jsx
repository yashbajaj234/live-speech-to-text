import { useCallback, useEffect, useRef, useState } from "react";
import useAudioRecorder from "./useAudioRecorder";
import useSocket from "./useSocket";
import { socket } from "./useSocket";
import "../public/styles.css"
// IMPORTANT: To ensure proper functionality and microphone access, please follow these steps:
// 1. Access the site using 'localhost' instead of the local IP address.
// 2. When prompted, grant microphone permissions to the site to enable audio recording.
// Failure to do so may result in issues with audio capture and transcription.
// NOTE: Don't use createPortal()



function App() {

  const [recordingState, setRecordingState] = useState(false);
  const [transcribedText, setTranscribedText] = useState("");
  const [copyBtn, setCopyBtn] = useState("Copy");
  const [partialData, setPartialData] = useState("");
  const prevFinalData = useRef("");

  useSocket();

  const toggleRecordingState = () => {
    setRecordingState(prev => !prev);
  }

  const onFinalTranscription = useCallback((finalData) => {
    if (finalData) {
      setPartialData(() => null);
      if (prevFinalData.current !== finalData) {
        setTranscribedText(prev => { const combined = prev + finalData; return combined.trim() });
        prevFinalData.current = finalData;
      }
    }
  }, [prevFinalData])

  const onPartialTransciption = (partialData) => {

    if (partialData)
      setPartialData(() => partialData ?? "")
  }


  // useEffect(() => {
  //   // Note: must connect to server on page load but don't start transcriber
  //   initialize();

  // }, [socket]);

  const { startRecording, stopRecording, isRecording } = useAudioRecorder({
    dataCb: (data) => {
      socket.emit("incoming-audio", data)
    },
  });


  const clear = () => {
    setTranscribedText('');
    setCopyBtn("Copy");
  }

  const copy = () => {

    navigator.clipboard.writeText(transcribedText);
    setCopyBtn("Copied");

  }


  const configureStream = () => {
    socket.emit("configure-stream", { sampleRate: 96000 })
  }

  const onStartRecordingPress = async () => {

    // start recorder and transcriber (send configure-stream)


    if (socket.connected) {
      console.log(socket.connected)
      toggleRecordingState();
      startRecording();
      socket.on("final", onFinalTranscription);
      socket.on("partial", onPartialTransciption)
    }

  };

  const onStopRecordingPress = async () => {

    socket.off("final", onFinalTranscription);
    socket.off("partial", onPartialTransciption)
    stopRecording();
    toggleRecordingState();
    socket.emit("stop-stream")

  }

  // ... add more functions
  return (
    <div>
      <h1>Speechify Voice Notes</h1>
      <p>Record or type something in the textbox.</p>
      {/* <p>{isRecording ? "Yes" : "No"}</p> */}

      <textarea value={transcribedText + (partialData ?? "")} onChange={(e) => setTranscribedText(e.target.value)} id="transcription-display" rows={5} cols={50}></textarea>
      <div className="btn-flex">
        <button id="record-button" className="btn" onDoubleClick={!recordingState ? onStartRecordingPress : null} onClick={recordingState ? onStopRecordingPress : configureStream}>{recordingState ? "Stop Recording" : "Start Recording"}</button>
        <button id="copy-button" className="btn" onClick={copy}>{copyBtn}</button>
        <button id="reset-button" className="btn" onClick={clear}>Clear</button>
      </div>
    </div >
  );
}

export default App;
