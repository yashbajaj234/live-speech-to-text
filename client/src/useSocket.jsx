import { useCallback, useEffect } from "react";
import io from "socket.io-client";

const serverURL = "http://localhost:8080";

const subscriptions = ["final", "partial", "transcriber-ready", "error"];

export const socket = io(serverURL, {
  autoConnect: false
});

// feel free to pass in any props
const useSocket = () => {



  useEffect(() => {
    initialize();

    return () => disconnect();
  }, [])

  const initialize = useCallback(() => {
    socket.connect();
  }, []);

  const disconnect = useCallback(() => {
    socket.disconnect();
  }, []);


  // socket.on('final')


  // ... free to add more functions
  return { socket };
};

export default useSocket;
