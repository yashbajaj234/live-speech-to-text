import EventEmitter from "events";
import { createClient, LiveTranscriptionEvents, LiveClient } from "@deepgram/sdk";

class Transcriber extends EventEmitter {
  //static deepgram = createClient(process.env.DEEPGRAM_API_KEY);
  constructor() {
    super();
    this.setMaxListeners(1); // Set maximum listeners for this instance
    this.live = null; // Initialize live transcription instance
    this.deepgram = createClient(process.env.DEEPGRAM_API_KEY)
  }

  // sampleRate: number
  startTranscriptionStream(sampleRate = 96000) {
    if (!this.live) {
      this.live = this.deepgram.listen.live(
        {
          model: "nova-2",
          punctuate: true,
          language: "en",
          interim_results: true,
          diarize: false,
          smart_format: true,
          endpointing: 0,
          encoding: "linear16",
          sample_rate: sampleRate,
          profanity_filter: true
        });

      this.live.on(LiveTranscriptionEvents.Open, () => {
        this.emit('transcriber-ready');
        this.live.on(LiveTranscriptionEvents.Transcript, (data) => {

          console.log(data.channel.alternatives[0].transcript, " Final: ", data?.["is_final"]);
          if (data?.["is_final"] === false)
            this.emit('partial', data.channel.alternatives[0].transcript);

          if (data?.["is_final"] === true)
            this.emit('final', data.channel.alternatives[0].transcript);
        });
      });

      // Example: Emit final transcription result

    }
  }

  endTranscriptionStream() {
    // close deepgram connection here
    if (this.live) {
      // Clean up live transcription instance
      this.live.finish();
      this.live = null; // Reset live instance
    }
  }

  // NOTE: deepgram must be ready before sending audio payload or it will close the connection
  send(payload) {
    if (this.live?.getReadyState()) {
      this.live.send(payload)
    } else {
      console.error('Live transcription not started. Call startTranscriptionStream() first.');
    }

  }

  // ... feel free to add more functions
}

export default Transcriber;
