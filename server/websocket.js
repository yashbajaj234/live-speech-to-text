import Transcriber from "./transcriber.js";

/**
 * Events to subscribe to:
 * - connection: Triggered when a client connects to the server.
 * - configure-stream: Requires an object with a 'sampleRate' property.
 * - incoming-audio: Requires audio data as the parameter.
 * - stop-stream: Triggered when the client requests to stop the transcription stream.
 * - disconnect: Triggered when a client disconnects from the server.
 *
 *
 * Events to emit:
 * - transcriber-ready: Emitted when the transcriber is ready.
 * - final: Emits the final transcription result (string).
 * - partial: Emits the partial transcription result (string).
 * - error: Emitted when an error occurs.
 */


const initializeWebSocket = (io) => {

  let transcriber = new Transcriber();

  io.on("connection", (socket) => {

    const emitFinalData = (data) => { if (data) socket.emit("final", data) };
    const emitPartialData = (data) => {
      if (data)
        socket.emit("partial", data)
    };

    const handleConfigureStream = (data) => {
      // console.log("configure", data);
      transcriber.startTranscriptionStream(data.sampleRate);
    };

    const handleIncomingAudio = (data) => {
      transcriber.send(data);
      transcriber.on("partial", emitPartialData);
      transcriber.on("final", emitFinalData);
    };

    const handleDisconnect = (reason) => {
      // console.log({ reason });
      // Clean up event listeners
      socket.off("configure-stream", handleConfigureStream);
      socket.off("incoming-audio", handleIncomingAudio);
      socket.off("disconnect", handleDisconnect);
      transcriber.off("partial", emitPartialData);
      transcriber.off("final", emitFinalData);
      socket.off("stop-stream", handleStopStream)

    };

    const handleStopStream = (data) => {
      // console.log("configure", data);
      transcriber.endTranscriptionStream();
    };

    socket.on("configure-stream", handleConfigureStream);
    socket.on("incoming-audio", handleIncomingAudio);
    socket.on("disconnect", handleDisconnect);
    socket.on("stop-stream", handleStopStream)
  });


};

export default initializeWebSocket;


